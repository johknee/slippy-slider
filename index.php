<!DOCTYPE html>
<html>
<head>
    <!-- - - META TAGS - - -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>Slider</title>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
    <!-- - - END META TAGS - - -->
    
    <!-- - - SCRIPTS - - -->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="slippyslider.js"></script>

    <!-- - - END SCRIPTS - - -->
    
    
    <!-- - - STYLESHEETS - - -->
    <link rel="stylesheet" href="slippyslider.css">
    <!-- - - END STYLESHEETS - - -->

    <style>
    * {
        padding:0px;
        margin:0px;
    }
    .wrap {
        max-width:900px;
        width:100%;
        margin:0 auto;
    }
    </style>
</head>

<body>

    <div class="wrap">
    	<ul>
            <li>
                <img src="images/1.jpg"/>
            </li>
            <li>
                <img src="images/2.jpg"/>
            </li>
            <li>
                <img src="images/3.jpg"/>
            </li>
            <li>
                <img src="images/4.jpg"/>
            </li>
            <li>
                <img src="images/5.jpg"/>
            </li>
        </ul>

        <div class="custom_arrows">
            <a href="#" class="arrow left">Left</a>
            <a href="#" class="arrow right">Right</a>
        </div>
        <div>
            <a href="javascript:slider.play();">Play</a>
            <a href="javascript:slider.pause();">Pause</a>
        </div>
    </div>

    <script>
    var slider;

    $(window).load(function()
    {
        slider = new SlippySlider($("ul"),{
            arrows : false,
            clickThrough : true,
            drag : true,
            loop : true,
            distance : 150,
            setHeight : true,
            play : true,
            delay : 5000,
            keyControl : true,
            arrow_selector : false,
            paginate : true,
            load : function(object){  },
            before : function(object){  },
            during : function(object){  },
            after : function(object){  }
        });
    });

    </script>

</body>
</html>