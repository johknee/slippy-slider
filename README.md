Slippy Slider
=====================

A little javascript slideshow, it needs jQuery and jQuery UI. It can use touch events too.

Usage
---------------------

```
slider = new SlippySlider($("ul"),{
    arrows : false,
    arrow_selector : false,
    clickThrough : true,
    drag : true,
    loop : true,
    distance : 150,
    setHeight : true,
    play : true,
    delay : 5000,
    keyControl : true,
    paginate : true,
    load : function(object){  },
    before : function(object){  },
    during : function(object){  },
    after : function(object){  }
});
```

Options
---------------------

**arrows** *(Boolean)*  
Yes or no to decide whether arrows will be displayed to cycle through the slideshow. If you want a custom arrow selector this must be TRUE.

**arrow_selector** *(Boolean:false / String)*  
Either false or a string of the selector name i.e *".arrow_wrapper"*. The container that you select must have 2 elements inside with the classes ".arrow.left" and ".arrow.right".

**clickThrough** *(Boolean)*  
Whether or not clicking the slideshow advances the slide.

**drag** *(Boolean)*  
Adds a drag element to the slide.

**loop** *(Boolean)*  
Whether or not the slideshow loops back round.

**distance** *(Integer)*  
Minimum distance to drag the slide before it triggers an advance.

**setHeight** *(Boolean:true / String:px-%)*  
If set to true the height will be set by the content. If a string with a pixel value or percentage value is given, the slider will be set to that height.

**play** *(Boolean)*  
Whether or not to autoplay the slider.

**delay** *(Integer)*  
Milliseconds before advancing autoplay.

**keyControl** *(Boolean)*  
Whether or not the keyboard arrows left and right control the slider.

**paginate** *(Boolean)*  
If true a dotted list gets added to control the positions.

**load** *(Function(object))*  
A function that gets fired on load of the slider.

**before** *(Function(object))*  
A function that gets fired before the slider moves.

**during** *(Function(object))*  
A function that gets fired everytime the slider is moved (During drag).

**after** *(Function(object))*  
A function that gets fired after the slider has moved.



API
---------------------

```
slider.next();
slider.previous();
slider.play();
slider.pause();
slider.moveSlide(index,noTrans); // noTrans true removes the class that adds CSS transitions, for immediate snapping
slider.returnObject();
```

Dependencies used:

* jQuery UI Touch Punch 0.2.3
* jquery.unevent.js 0.2
* http://davidwalsh.name/css-animation-callback