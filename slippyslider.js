var SlippySlider = function(el,prop)
{
	if(!window.jQuery||!window.jQuery.ui)
	{
		console.log("Slippy Slider needs jQuery and jQuery UI");
	}
	else
	{
		el.wrap("<div class='slider_wrapper'></div>");

		this.element 			= el.parent(".slider_wrapper");
		this.slider 			= el;

		this.total				= el.find("li").length;
		this.position			= 0;
		this.last 				= 0;
		this.x 					= 0;
		this.transitioning 		= false;
		this.transitionEvent 	= whichTransitionEvent();

		this.startX 	= 0;

		this.properties = {
			arrows : false,
			clickThrough : true,
			drag : true,
			loop : true,
			distance : 150,
			setHeight : true,
			play : true,
			delay : 5000,
			keyControl : true,
			arrow_selector : false,
			paginate : true,
            load : function(){  },
            before : function(){  },
            during : function(){  },
            after : function(){  }
		};

		$.extend(this.properties,prop);

		this.autoplayer 	= setTimeout(function(){ },250);

		this.height 		= 0;
		this.width 			= 0;
		this.totalWidth 	= 0;

		this.addElements();
		this.addListeners();
		this.setup();

		var object = this.returnObject();
		this.properties.load(object);

		if(this.properties.play)
		{
			this.autoplay();
		}
	}
};

SlippySlider.prototype.setup = function()
{
	var copy = this;
	var maxHeight = 0;

	copy.slider.find("li").not(".clone").each(function(index,element)
	{
		if(copy.properties.setHeight==true)
		{
			var hei = $(this).find("img").height();

			if(hei>maxHeight)
			{
				maxHeight = hei;
			}
		}

		$(this).css({"left":(100*index)+"%"});
	});

	copy.addClones();

	if(copy.properties.setHeight!=true)
	{
		maxHeight = copy.properties.setHeight;
	}

	copy.height = maxHeight;
	copy.element.height(copy.height);
	copy.width = this.element.width();
	copy.totalWidth = copy.width * copy.total;

	copy.slider.find("li").removeClass("active");
	copy.slider.find("li:not(.clone):eq("+copy.position+")").addClass("active");
};

SlippySlider.prototype.autoplay = function()
{
	var copy = this;

	clearTimeout(this.autoplayer);

	this.autoplayer = setTimeout(function()
	{
		copy.next();
	},copy.properties.delay);	
};

SlippySlider.prototype.addClones = function()
{
	if(this.properties.loop)
	{
		this.slider.find("li.clone").remove();
		var first = this.slider.find("li:eq(0)").clone(),
			last = this.slider.find("li:eq("+(this.total-1)+")").clone();

		first.addClass("clone").css({
			"left"	: (this.total*100)+"%",
		});

		last.addClass("clone").css({
			"left"	: "-100%",
		});

		this.slider.append(first).prepend(last);


		var first = this.slider.find("li:eq(1)").clone(),
			last = this.slider.find("li:eq("+(this.total-2)+")").clone();

		first.addClass("clone").css({
			"left"	: ((this.total+1)*100)+"%",
		});

		last.addClass("clone").css({
			"left"	: "-200%",
		});

		this.slider.append(first).prepend(last);
	}
};

SlippySlider.prototype.map = function(value, low1, high1, low2, high2)
{
    return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
};

SlippySlider.prototype.play = function()
{
	this.properties.play = true;
	this.autoplay();
};

SlippySlider.prototype.pause = function()
{
	clearTimeout(this.autoplayer);
};

SlippySlider.prototype.next = function()
{
	clearTimeout(this.autoplayer);
	if(!this.transitioning)
	{
		var pos = this.position;

		pos += 1;

		if(pos>=this.total&&!this.properties.loop)
		{
			this.moveSlide(this.total-1);
		}
		else
		{
			this.moveSlide(pos);
		}
	}
};

SlippySlider.prototype.previous = function()
{
	clearTimeout(this.autoplayer);
	if(!this.transitioning)
	{
		var pos = this.position;

		pos -= 1;

		if(pos<0&&!this.properties.loop)
		{
			this.moveSlide(0);
		}
		else
		{
			this.moveSlide(pos);
		}
	}
};

SlippySlider.prototype.moveSlide = function(index,noTrans)
{
	if(this.position!=index)
	{
		this.last = this.position;
		var object = this.returnObject();
		this.properties.before(object);
	}

	this.slider.find("li").not(".clone").eq(this.position).removeClass("active");
	this.position = index;

	this.slider.find("li").not(".clone").eq(this.position).addClass("active");

	this.x = ((index*this.width)*-1);
	this.slidePos(this.x,noTrans);

	if(this.properties.play)
	{
		this.autoplay();
	}
};

SlippySlider.prototype.slidePos = function(pos,noTrans)
{
	var copy = this;

	if(noTrans)
	{
		this.slider.removeClass("transition");
		copy.slider.css({
			"left"	: pos+"px"
		});

		setTimeout(function()
		{
			copy.slider.addClass("transition");
		},10);
	}
	else
	{
		copy.transitioning = true;
		this.slider.css({
			"left"	: pos+"px"
		});
	}
};

SlippySlider.prototype.returnObject = function()
{
	var obj = {};

	obj.active 		= this.slider.find("li").not(".clone").eq(this.position);
	obj.index 		= this.position;
	obj.last 		= this.last;
	obj.num			= this.position + 1;
	obj.total 		= this.total;
	obj.height 		= this.height;
	obj.width 		= this.width;
	obj.fullWidth 	= this.totalWidth;

	return obj;
}

SlippySlider.prototype.addListeners = function()
{
	var copy = this;


	copy.slider.one(copy.transitionEvent, function(event) {
		if(copy.transitioning)
		{
			var object = copy.returnObject();
			
			if(object.index>=object.total)
			{
				copy.position = 0;
				object = copy.returnObject();
				copy.moveSlide(copy.position,true)
			}

			if(object.index<0)
			{
				copy.position = copy.total - 1;
				object = copy.returnObject();
				copy.moveSlide(copy.position,true)
			}

			copy.transitioning = false;

			var object = copy.returnObject();
			console.log("after fired");
			copy.properties.after(object);

			copy.paginate(object.index);
		}
	});

	if(copy.properties.clickThrough)
	{
		copy.element.click(function()
		{
			copy.next();
		});
	}

	if(copy.properties.keyControl)
	{
		$(window).keyup(function(e)
		{
			var arrow = { left : 37, right : 39};

			switch(e.keyCode)
			{
				case arrow.left:
					copy.previous();
					break;
				case arrow.right:
					copy.next();
					break;
			}
		});
	}


	if(copy.properties.drag)
	{
		copy.slider.draggable({
			axis : "x",
			start : function(event,ui)
			{
				$(ui.helper).removeClass("transition");
			},
			drag : function(event,ui)
			{
				var object = copy.returnObject();
				copy.properties.during(object);
				copy.transitioning = false;
			},
			stop : function(event,ui)
			{
				var dir = ui.originalPosition.left - ui.position.left;
				var abs = Math.abs(dir);

				$(ui.helper).addClass("transition");
				var pos = ui.position.left;
				copy.x = pos;

				if(abs>copy.properties.distance)
				{
					if(dir<0)
					{
						copy.previous();
					}
					else
					{
						copy.next();
					}
				}
				else
				{
					copy.moveSlide(copy.position);
				}
			}
		});
	}

	$(window).resize(function()
	{
		copy.setup();
	});
	$(window).on("resize",function()
	{
		copy.moveSlide(copy.position);
	},250);

	if(copy.properties.arrows)
	{
		var targ = {
			left : copy.element.find(".arrow.left"),
			right : copy.element.find(".arrow.right")
		};

		if(copy.properties.arrow_selector!=false)
		{
			targ = {
				left : copy.properties.arrow_selector.find(".arrow.left"),
				right : copy.properties.arrow_selector.find(".arrow.right")
			};
		}

		targ.left.click(function(e)
		{
			e.preventDefault();
			copy.previous();
			return false;
		});

		targ.right.click(function(e)
		{
			e.preventDefault();
			copy.next();
			return false;
		});
	}
};

SlippySlider.prototype.addElements = function()
{
	var copy = this;

	this.slider.addClass("transition");
	this.slider.addClass("slider");

	if(this.properties.arrows&&this.properties.arrow_selector==false)
	{
		this.element.prepend("<a href='#' class='arrow left'>Left</a><a href='#' class='arrow right'>Right</a>");
	}

	if(this.properties.paginate)
	{
		this.element.prepend("<ul class='slider_pagination'></ul>");

		for(var i = 0; i < this.total; i++)
		{
			if(i == 0)
			{
				this.element.find(".slider_pagination").append("<li class='dot active'>"+(i+1)+"<li>");
			}
			else
			{
				this.element.find(".slider_pagination").append("<li class='dot'>"+(i+1)+"</li>");
			}
		}

		this.element.find(".dot").click(function(e)
		{
			e.preventDefault();
			var ind = copy.element.find(".dot").index(this);
			copy.moveSlide(ind);
			return false;
		});
	}
};

SlippySlider.prototype.paginate = function(index)
{
	if(this.properties.paginate)
	{
		this.element.find(".dot.active").removeClass("active");
		this.element.find(".dot:eq("+index+")").addClass("active");
	}
};

/*!
 * jQuery UI Touch Punch 0.2.3
 *
 * Copyright 2011–2014, Dave Furfero
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 * Depends:
 *  jquery.ui.widget.js
 *  jquery.ui.mouse.js
 */
!function(a){function f(a,b){if(!(a.originalEvent.touches.length>1)){/*a.preventDefault();*/var c=a.originalEvent.changedTouches[0],d=document.createEvent("MouseEvents");d.initMouseEvent(b,!0,!0,window,1,c.screenX,c.screenY,c.clientX,c.clientY,!1,!1,!1,!1,0,null),a.target.dispatchEvent(d)}}if(a.support.touch="ontouchend"in document,a.support.touch){var e,b=a.ui.mouse.prototype,c=b._mouseInit,d=b._mouseDestroy;b._touchStart=function(a){var b=this;!e&&b._mouseCapture(a.originalEvent.changedTouches[0])&&(e=!0,b._touchMoved=!1,f(a,"mouseover"),f(a,"mousemove"),f(a,"mousedown"))},b._touchMove=function(a){e&&(this._touchMoved=!0,f(a,"mousemove"))},b._touchEnd=function(a){e&&(f(a,"mouseup"),f(a,"mouseout"),this._touchMoved||f(a,"click"),e=!1)},b._mouseInit=function(){var b=this;b.element.bind({touchstart:a.proxy(b,"_touchStart"),touchmove:a.proxy(b,"_touchMove"),touchend:a.proxy(b,"_touchEnd")}),c.call(b)},b._mouseDestroy=function(){var b=this;b.element.unbind({touchstart:a.proxy(b,"_touchStart"),touchmove:a.proxy(b,"_touchMove"),touchend:a.proxy(b,"_touchEnd")}),d.call(b)}}}(jQuery);















/*!
 * jquery.unevent.js 0.2
 * https://github.com/yckart/jquery.unevent.js
 *
 * Copyright (c) 2013 Yannick Albert (http://yckart.com)
 * Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php).
 * 2013/07/26
**/
;(function ($) {
    var on = $.fn.on, timer;
    $.fn.on = function () {
        var args = Array.apply(null, arguments);
        var last = args[args.length - 1];

        if (isNaN(last) || (last === 1 && args.pop())) return on.apply(this, args);

        var delay = args.pop();
        var fn = args.pop();

        args.push(function () {
            var self = this, params = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
                fn.apply(self, params);
            }, delay);
        });

        return on.apply(this, args);
    };
}(this.jQuery || this.Zepto));


// Function from David Walsh: http://davidwalsh.name/css-animation-callback
function whichTransitionEvent(){
  var t,
      el = document.createElement("fakeelement");

  var transitions = {
    "transition"      : "transitionend",
    "OTransition"     : "oTransitionEnd",
    "MozTransition"   : "transitionend",
    "WebkitTransition": "webkitTransitionEnd"
  }

  for (t in transitions){
    if (el.style[t] !== undefined){
      return transitions[t];
    }
  }
}







